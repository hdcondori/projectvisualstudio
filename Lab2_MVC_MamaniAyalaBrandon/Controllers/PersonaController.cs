﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Lab2_MVC_MamaniAyalaBrandon.Models;


namespace Lab2_MVC_MamaniAyalaBrandon.Controllers
{
    public class PersonaController : Controller
    {
        List<ClsPersona> obj_Lista_Persona = new List<ClsPersona>();

        // GET: Persona
        public ActionResult Index()
        {
          
            //Se instancia con persona1
            ClsPersona objPersona1 = new ClsPersona
            {
                //Instanciar los atributos de la clase
                apellido = "Mamani Ayala",
                nombre = "Brandon",
                email = "bramamania@upt.pe",
                sexo = true,
                edad = 22,
                altura = 1.71
            };
            //Pasar el objeto al objeto lista
            obj_Lista_Persona.Add(objPersona1);

            ClsPersona objPersona2 = new ClsPersona
            {
                apellido = "Sosa Bedoya",
                nombre = "Sharon",
                email = "sharon123@hotmail.com",
                sexo = false,
                edad = 21,
                altura =1.61
            };

            obj_Lista_Persona.Add(objPersona2);

            ClsPersona objPersona3 = new ClsPersona
            {
                apellido = "Ordoñez Quilli",
                nombre = "Ronald",
                email = "ron123@hotmail.com",
                sexo = true,
                edad = 25,
                altura = 1.75
            };

            obj_Lista_Persona.Add(objPersona3);



            return View(obj_Lista_Persona);
        }
    }
}