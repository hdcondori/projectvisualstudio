﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Lab2_MVC_MamaniAyalaBrandon.Models;

namespace Lab2_MVC_MamaniAyalaBrandon.Models
{
    public class ClsBisiesto
    {
        public int Inicio { get; set; }
        public int Fin { get; set; }
        public string Bisiesto { get; set; }
        public string Nobisiesto { get; set; }
        public string Todo { get; set; }
        public string Resultado { get; set; }
    }
}